<?php
return array(
    'errorHandler' => array(
        'server' => 'server-name',
        'transport' => array(
            'type' => 'slack',
            'params' => array(
                'hookUrl' => 'https://hooks.slack.com',
                'botName' => 'botName',
                'chanel' => '#chanel',
            )
        ),
    ),
    'ignoreErrors' => array(
        'stat()'
    ),
 );
