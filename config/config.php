<?php
return array(
    'mysqlToGoogleBigQuery' => array(
        'projectId' => '',
        'keyFilePath' => '',
        'dataSet' => '',
        'databaseConfig' => array(
            'driver'   => 'pdo_mysql',
            'user'     => 'root',
            'password' => '',
            'dbname'   => '',
            'charset' => 'UTF8',
        ),
        'merger' => array(
          'packageSize' => 1000
        ),
        'updater' => array(
          'packageSize' => 100,
          'iterationCount' => 1,
          'tables' => array('tb_clients')
        ),
        'fromDataTypeToDataTypes' => array(
            'TINYINT' => 'INT64',
            'SMALLINT' => 'INT64',
            'MEDIUMINT' => 'INT64',
            'INT' => 'INT64',
            'BIGINT' => 'INT64',
            'DECIMAL' => 'BIGNUMERIC',
            'FLOAT' => 'FLOAT64',
            'DOUBLE' => 'FLOAT64',
            'BIT' => 'BOOL',
            'CHAR' => 'STRING',
            'VARCHAR' => 'STRING',
            'BINARY' => 'BYTES',
            'VARBINARY' => 'BYTES',
            'TINYTEXT' => 'STRING',
            'TEXT' => 'STRING',
            'MEDIUMTEXT' => 'STRING',
            'LONGTEXT' => 'STRING',
            'ENUM' => 'STRING',  //No type for ENUM.Must use any type which can represent values in ENUM
            'SET' => 'STRING',  //No type for SET.Must use any type which can represent values in SET
            'DATE' => 'DATE',
            'TIME' => 'TIME',
            'DATETIME' => 'DATETIME',
            'TIMESTAMP' => 'TIMESTAMP'
        ),
        'slackErrorNotifier' => require(dirname(__FILE__) . '/slack-error-notifier-config.php'),
        'restLogService' => require(dirname(__FILE__) . '/rest-log-service-config.php'),
        'tables' => array(
            'tb_clients' => array(
                'query' => 'SELECT * FROM `bq_clients` WHERE code=:code',
                'queryParams' => array(
                    'code' => '1',
                    'companyId' => 2
                ),
                'table' => 'bq_clients',
                'description' => 'Client table',
                'columns' => array(
                    'id' => array(
                        'description' => ''
                    ),
                    'code' => array(
                        'description' => 'Unique identifier on your system '
                    ),
                    'name' => array(
                        'name' => 'title',
                        'description' => 'Client name'
                    ),
                )
            ),
        ),
    )
);