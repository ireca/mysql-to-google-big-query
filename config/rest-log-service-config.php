<?php
return array(
    'restLog' => array(
        'isActive' => true,
        'maxCodeCount' => 1,
        'databaseConfig' => require(dirname(__FILE__) . '/database-config.php'),
    )
);
