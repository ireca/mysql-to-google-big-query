<?php
namespace MysqlToGoogleBigQuery\Provider;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;

class DatabaseProvider extends EntityManagerProvider
{
    /**
     * @param array $dbTableNames
     *
     * @return array
     * @throws DBALException|ORMException
     */
    public function getSchemaInfo(array $dbTableNames)
    {
        if (count($dbTableNames) == 0) {

            return array();
        }

        $tableNameAsString = implode($dbTableNames, '","');
        $sql = "
            SELECT
             `TABLE_NAME`, 
             `COLUMN_NAME`, 
             `DATA_TYPE`,
             `IS_NULLABLE`,
             `CHARACTER_MAXIMUM_LENGTH`
            FROM
                INFORMATION_SCHEMA.COLUMNS
            WHERE
                `TABLE_SCHEMA`=:database AND `TABLE_NAME` IN(\"$tableNameAsString\")
            ORDER BY TABLE_NAME
        ";
        $stmt = $this->getEm()->getConnection()->prepare($sql);
        $stmt->bindValue('database', $this->getEm()->getConnection()->getDatabase());
        $stmt->execute();

        return $stmt->fetchAll();
    }
    
}