<?php
namespace MysqlToGoogleBigQuery\Provider;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

abstract class EntityManagerProvider
{
    /**
     * @var array
     */
    private $databaseConfig = array();

    /**
     * @var EntityManager
     */
    private $em = null;

    /**
     * @var string
     */
    private $errorMessage = '';

    /**
     * EntityManagerProvider constructor.
     *
     * @param array $databaseConfig
     */
    public function __construct(array $databaseConfig)
    {
        $this->databaseConfig = $databaseConfig;
    }

    /**
     * @return EntityManager|null
     * @throws \Doctrine\ORM\ORMException
     */
    public function getEm()
    {
        if (is_null($this->em) || ($this->em->isOpen() == false)) {
            $config = Setup::createAnnotationMetadataConfiguration(
                array("/data"),
                true,
                null,
                null,
                false
            );
            $this->em = EntityManager::create($this->databaseConfig, $config);
        }

        return $this->em;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

}