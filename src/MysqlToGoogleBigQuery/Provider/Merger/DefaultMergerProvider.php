<?php
namespace MysqlToGoogleBigQuery\Provider\Merger;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\ResultSetMapping;
use MysqlToGoogleBigQuery\Exception\LoggedException;
use MysqlToGoogleBigQuery\Provider\Result\IterationResult;
use MysqlToGoogleBigQuery\Provider\Result\TableValueResult;

class DefaultMergerProvider extends MergerBaseProvider implements MergerProviderInterface
{
    /**
     * @param $lastId
     * @return IterationResult
     */
    public function getIterationRowCount($lastId)
    {
        $sql = "SELECT COUNT(*) AS 'rowCount', MAX(id) AS 'maxId', MIN(id) AS 'minId' FROM `%s` WHERE `id`>:lastId";
        $sql = sprintf($sql, $this->getDbTable());
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('rowCount', 'rowCount', 'integer');
        $rsm->addScalarResult('maxId', 'maxId', 'integer');
        $rsm->addScalarResult('minId', 'minId', 'integer');
        $result = $this
            ->getEm()
            ->createNativeQuery($sql, $rsm)
            ->setParameter('lastId', $lastId)
            ->getResult(AbstractQuery::HYDRATE_SCALAR);
        if (! isset($result[0])) {
            return new IterationResult(0, 0, 0);
        }

        return new IterationResult(
            $result[0]['rowCount'],
            ! is_null($result[0]['minId']) ? $result[0]['minId'] : $lastId,
            ! is_null($result[0]['maxId']) ? $result[0]['maxId'] : $lastId
        );
    }

    /**
     * @param $lastId
     * @param $limit
     * @return TableValueResult
     * @throws LoggedException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getRows($lastId, $limit)
    {
        $sql = "SELECT %s FROM `%s` WHERE `id`>:lastId ORDER BY `id` ASC LIMIT :limit";
        $sql = sprintf($sql, $this->getColumnRequestList()->getColumnsAsString(), $this->getDbTable());
        $query = $this
            ->getEm()
            ->getConnection()
            ->prepare($sql);
        $query->bindValue('lastId', $lastId, Type::INTEGER);
        $query->bindValue('limit', $limit, Type::INTEGER);
        if ($query->execute() === false) {
            throw new LoggedException("I can't execute query:" . print_r($query->errorInfo(), true));
        }

        return new TableValueResult($query->fetchAll());
    }

}