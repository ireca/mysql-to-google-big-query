<?php
namespace MysqlToGoogleBigQuery\Provider\Merger;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\ResultSetMapping;
use MysqlToGoogleBigQuery\Exception\LoggedException;
use MysqlToGoogleBigQuery\Provider\Result\IterationResult;
use MysqlToGoogleBigQuery\Provider\Result\TableValueResult;

class CustomMergerProvider extends MergerBaseProvider implements MergerProviderInterface
{
    /**
     * @param $lastId
     * @return IterationResult
     * @throws LoggedException
     */
    public function getIterationRowCount($lastId)
    {
        $sql = $this->getColumnRequestList()->getQuery();
        $this->validateCustomSql($sql);
        $sql = sprintf($sql, "COUNT(*) AS 'rowCount'"). " AND `{$this->getDbTable()}`.`id`>:lastId";

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('rowCount', 'rowCount', 'integer');
        $query = $this->getEm()->createNativeQuery($sql, $rsm);
        $query->setParameter('lastId', $lastId);
        foreach ($this->getColumnRequestList()->getQueryParamList()->getValues() as $queryParam) {
            $query->setParameter(
                $queryParam->getName(),
                $queryParam->getValue(),
                $queryParam->getCnqType()
            );
        }
        $mamResult = $this->getMamResult($lastId);

        return new IterationResult(
            $query->getResult(AbstractQuery::HYDRATE_SINGLE_SCALAR),
            $mamResult->minId,
            $mamResult->maxId
        );
    }

    /**
     * @param $lastId
     * @return \stdClass
     */
    private function getMamResult($lastId)
    {
        $mamResult = new \stdClass();
        $mamResult->maxId = $lastId;
        $mamResult->minId = $lastId;

        $sql = "SELECT MIN(id) AS 'minId', MAX(id) AS 'maxId' FROM `%s` WHERE `id`>:lastId";
        $sql = sprintf($sql, $this->getDbTable());
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('maxId', 'maxId', 'integer');
        $rsm->addScalarResult('minId', 'minId', 'integer');
        $result = $this
            ->getEm()
            ->createNativeQuery($sql, $rsm)
            ->setParameter('lastId', $lastId)
            ->getResult(AbstractQuery::HYDRATE_SCALAR);
        if (! isset($result[0])) {

            return $mamResult;
        } else {
            $mamResult->maxId = ! is_null($result[0]['maxId']) ? $result[0]['maxId'] : $lastId;
            $mamResult->minId = ! is_null($result[0]['minId']) ? $result[0]['minId'] : $lastId;
        }

        return $mamResult;
    }

    /**
     * @param $lastId
     * @param $limit
     * @return TableValueResult
     * @throws LoggedException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getRows($lastId, $limit)
    {
        $sql = $this->getColumnRequestList()->getQuery();
        $this->validateCustomSql($sql);
        $sql = sprintf($sql, $this->getColumnRequestList()->getColumnsAsString())
            . " AND `{$this->getDbTable()}`.`id`>:lastId ORDER BY `{$this->getDbTable()}`.`id` LIMIT :limit";

        $query = $this
            ->getEm()
            ->getConnection()
            ->prepare($sql);
        $query->bindValue('lastId', $lastId, Type::INTEGER);
        $query->bindValue('limit', $limit, Type::INTEGER);
        foreach ($this->getColumnRequestList()->getQueryParamList()->getValues() as $queryParam) {
            $query->bindValue(
                $queryParam->getName(),
                $queryParam->getValue(),
                $queryParam->getType()
            );
        }

        if ($query->execute() === false) {
            throw new LoggedException("I can't execute query:" . print_r($query->errorInfo(), true));
        }

        return new TableValueResult($query->fetchAll());
    }


}