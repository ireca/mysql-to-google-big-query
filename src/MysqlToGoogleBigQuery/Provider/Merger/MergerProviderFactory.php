<?php
namespace MysqlToGoogleBigQuery\Provider\Merger;

use Doctrine\ORM\EntityManager;
use MysqlToGoogleBigQuery\Request\ColumnRequestList;

class MergerProviderFactory
{
    /**
     * @param EntityManager $em
     * @param ColumnRequestList $columnRequestList
     * @return CustomMergerProvider|DefaultMergerProvider
     */
    public static function create(EntityManager $em, ColumnRequestList $columnRequestList)
    {
        return empty($columnRequestList->getQuery())
            ? new DefaultMergerProvider($em, $columnRequestList)
            : new CustomMergerProvider($em, $columnRequestList);
    }

}