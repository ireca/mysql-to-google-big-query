<?php
namespace MysqlToGoogleBigQuery\Provider\Updater;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\ResultSetMapping;
use MysqlToGoogleBigQuery\Exception\LoggedException;
use MysqlToGoogleBigQuery\Provider\Result\TableValueResult;

class DefaultUpdaterProvider extends BaseUpdaterProvider implements UpdaterProviderInterface
{
    /**
     * @param $firstId
     * @param $lastId
     * @return array|float|int|string
     */
    public function getIterationRowCount($firstId, $lastId)
    {
        $sql = "SELECT COUNT(*) AS 'rowCount' FROM `%s` WHERE `id`>:firstId AND `id`<:lastId ORDER BY `id` ASC";
        $sql = sprintf($sql, $this->getDbTable());
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('rowCount', 'rowCount', 'integer');
        return $this
            ->getEm()
            ->createNativeQuery($sql, $rsm)
            ->setParameter('firstId', $firstId, Type::INTEGER)
            ->setParameter('lastId', $lastId)
            ->getResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);
    }

    /**
     * @param $lastId
     * @param $firstId
     * @return TableValueResult
     * @throws LoggedException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getRows($firstId, $lastId)
    {
        $sql = "SELECT %s FROM `%s` WHERE `id`>:firstId AND `id`<:lastId ORDER BY `id` ASC";
        $sql = sprintf($sql, $this->getColumnRequestList()->getColumnsAsString(), $this->getDbTable());
        $query = $this
            ->getEm()
            ->getConnection()
            ->prepare($sql);
        $query->bindValue('firstId', $firstId, Type::INTEGER);
        $query->bindValue('lastId', $lastId, Type::INTEGER);
        if ($query->execute() === false) {
            throw new LoggedException("I can't execute query:" . print_r($query->errorInfo(), true));
        }

        return new TableValueResult($query->fetchAll());
    }

}