<?php
namespace MysqlToGoogleBigQuery\Provider\Updater;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\ResultSetMapping;
use MysqlToGoogleBigQuery\Exception\LoggedException;

use MysqlToGoogleBigQuery\Provider\Result\TableValueResult;

class CustomUpdaterProvider extends BaseUpdaterProvider implements UpdaterProviderInterface
{

    /**
     * @param $firstId
     * @param $lastId
     * @return array|float|int|string
     * @throws LoggedException
     */
    public function getIterationRowCount($firstId, $lastId)
    {
        $sql = $this->getColumnRequestList()->getQuery();
        $this->validateCustomSql($sql);
        $sql = sprintf($sql, "COUNT(*) AS 'rowCount'") .
            sprintf(" AND `%s`.`id`>=:firstId AND `%s`.`id`<=:lastId", $this->getDbTable(), $this->getDbTable());
        $sql = $this->addExcludeIdsCondition($sql, $firstId, $lastId);

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('rowCount', 'rowCount', 'integer');
        $query = $this->getEm()->createNativeQuery($sql, $rsm);
        $query->setParameter('firstId', $firstId);
        $query->setParameter('lastId', $lastId);
        foreach ($this->getColumnRequestList()->getQueryParamList()->getValues() as $queryParam) {
            $query->setParameter(
                $queryParam->getName(),
                $queryParam->getValue()
            );
        }

        return $query->getResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);
    }

    /**
     * @param $firstId
     * @param $lastId
     * @return TableValueResult
     * @throws LoggedException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getRows($firstId, $lastId)
    {
        $sql = $this->getColumnRequestList()->getQuery();
        $this->validateCustomSql($sql);
        $sql = sprintf($sql, $this->getColumnRequestList()->getColumnsAsString()) .
            sprintf(" AND `%s`.`id`>=:firstId AND `%s`.`id`<=:lastId", $this->getDbTable(), $this->getDbTable());
        $sql = $this->addExcludeIdsCondition($sql, $firstId, $lastId);
        $sql = sprintf($sql . " ORDER BY `%s`.`id` ASC", $this->getDbTable());

        $query = $this
            ->getEm()
            ->getConnection()
            ->prepare($sql);
        $query->bindValue('lastId', $lastId, Type::INTEGER);
        $query->bindValue('firstId', $firstId, Type::INTEGER);
        foreach ($this->getColumnRequestList()->getQueryParamList()->getValues() as $queryParam) {
            $query->bindValue(
                $queryParam->getName(),
                $queryParam->getValue(),
                $queryParam->getType()
            );
        }

        if ($query->execute() === false) {
            throw new LoggedException("I can't execute query:" . print_r($query->errorInfo(), true));
        }

        return new TableValueResult($query->fetchAll());
    }

}