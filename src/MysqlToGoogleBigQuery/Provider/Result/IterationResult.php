<?php

namespace MysqlToGoogleBigQuery\Provider\Result;

class IterationResult
{
    private $rowCount = 0;

    private $minId = 0;

    private $maxId = 0;

    /**
     * @param int $countRows
     * @param int $maxId
     */
    public function __construct($countRows, $minId, $maxId)
    {
        $this->rowCount = $countRows;
        $this->minId = $minId;
        $this->maxId = $maxId;
    }

    /**
     * @return int
     */
    public function getRowCount()
    {
        return $this->rowCount;
    }

    /**
     * @return int
     */
    public function getMinId()
    {
        return $this->minId;
    }

    /**
     * @return int
     */
    public function getMaxId()
    {
        return $this->maxId;
    }

}