<?php
namespace MysqlToGoogleBigQuery\Provider\Result;

class TableValueResult
{
    private $values = array();
    
    /**
     * TableValueResult constructor.
     *
     * @param array $values
     */
    public function __construct(array $values)
    {
        $this->values = $values;
    }

    /**
     * @return array
     */
    public function getData()
    {
        $data = [];
        foreach ($this->getValues() as $value) {
            $data[] = array('data' => $value);
        }

        return $data;
    }

    /**
     * @return int|mixed
     */
    public function getValueLastId()
    {
        return isset(end($this->values)['id']) ? end($this->values)['id'] : 0;
    }
    
    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param array $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

}