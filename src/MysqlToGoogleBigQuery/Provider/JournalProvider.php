<?php
namespace MysqlToGoogleBigQuery\Provider;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\ResultSetMapping;
use MysqlToGoogleBigQuery\Model\Journal;
use MysqlToGoogleBigQuery\Provider\Result\JournalItemResult;

class JournalProvider extends EntityManagerProvider
{

    /**
     * @param $commandType
     * @param $table
     * @param $firstPk
     * @param $lastId
     * @param $itemCount
     * @param string $description
     * @param bool $isSuccess
     * @param string $errorMessage
     * @return true
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create($commandType, $table, $firstPk, $lastId, $itemCount, $description = '', $isSuccess = true, $errorMessage = '')
    {
        $journal = new Journal();
        $journal->commandType = $commandType;
        $journal->tableName = $table;
        $journal->firstPk = $firstPk;
        $journal->lastPk = $lastId;
        $journal->itemCount = $itemCount;
        $journal->isSuccess = $isSuccess;
        $journal->errorMessage = $errorMessage;
        $journal->createdAt = new \DateTime();
        $journal->description = $description;
        $this->getEm()->persist($journal);
        $this->getEm()->flush();        
        
        return true;
    }

    /**
     * @param array $tables
     * @param array $commandTypes
     * @param $iteration
     * @return JournalItemResult
     * @throws \Doctrine\ORM\ORMException
     */
    public function getItemsByIteration(array $tables, array $commandTypes, $iteration)
    {
        $queries = array();
        foreach ($tables as $table) {
            $queries[] =
                sprintf('SELECT * FROM
                    (SELECT 
                        * 
                     FROM 
                         `mq_journal` 
                     WHERE 
                        `tableName`="%s" AND `commandType` IN(:commandTypes)
                     ORDER BY `id` DESC
                     LIMIT :iteration, :limit) %s ' . "\r\n",
                    $table,
                    "alias_" . $table
                );
        }
        $finishQuery = implode(" UNION ALL \r\n", $queries);

        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Journal::class, 'j');
        $rsm->addFieldResult('j', 'id', 'id');
        $rsm->addFieldResult('j', 'tableName', 'tableName');
        $rsm->addFieldResult('j', 'lastPk', 'lastPk');
        $rsm->addFieldResult('j', 'firstPk', 'firstPk');
        $rsm->addFieldResult('j', 'createdAt', 'createdAt');
        $rsm->addIndexByScalar('tableName');
        $rsm->addIndexBy('tableName', 'tableName');
        $items =
            $this
                ->getEm()
                ->createNativeQuery($finishQuery, $rsm)
                ->setParameter('commandTypes', $commandTypes)
                ->setParameter('iteration', $iteration)
                ->setParameter('limit', 1)
                ->getResult();


        return new JournalItemResult($items);
    }

    /**
     * @param array $commandTypes
     * @param array $tables
     * @return JournalItemResult
     * @throws \Doctrine\ORM\ORMException
     */
    public function getLastJournalItems(array $commandTypes, array $tables)
    {
        $sql =
            'SELECT
               `id`, 
               `tableName`, 
               `lastPk`, 
               `firstPk`,
               `createdAt`
            FROM
              `mq_journal`
            WHERE id IN (
                SELECT 
                    MAX(id) 
                FROM  `mq_journal` 
                WHERE 
                    `tableName` IN (:tables) AND `commandType` IN (:commandTypes)
                GROUP BY `tableName`
            )';

        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Journal::class, 'j');
        $rsm->addFieldResult('j', 'id', 'id');
        $rsm->addFieldResult('j', 'tableName', 'tableName');
        $rsm->addFieldResult('j', 'firstPk', 'firstPk');
        $rsm->addFieldResult('j', 'lastPk', 'lastPk');
        $rsm->addFieldResult('j', 'createdAt', 'createdAt');
        $rsm->addIndexByScalar('tableName');
        $rsm->addIndexBy('tableName', 'tableName');
        $items =
            $this
                ->getEm()
                ->createNativeQuery($sql, $rsm)
                ->setParameter('tables', $tables)
                ->setParameter('commandTypes', $commandTypes)
                ->getResult();

        return new JournalItemResult($items);
    }

}