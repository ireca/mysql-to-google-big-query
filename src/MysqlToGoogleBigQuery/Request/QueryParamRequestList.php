<?php
namespace MysqlToGoogleBigQuery\Request;

class QueryParamRequestList
{
    /**
     * @var QueryParamRequest[]
     */
    private $items = array();

    /**
     * @param QueryParamRequest[] $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * @return QueryParamRequest[]
     */
    public function getItems()
    {
        return $this->items;
    }


}