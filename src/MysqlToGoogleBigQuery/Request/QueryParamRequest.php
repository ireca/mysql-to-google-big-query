<?php
namespace MysqlToGoogleBigQuery\Request;

class QueryParamRequest
{
    public $name = '';

    public $value = '';

    public $type = '';

    /**
     * @param string $name
     * @param string $value
     * @param string $type
     */
    public function __construct($name, $value, $type)
    {
        $this->name = $name;
        $this->value = $value;
        $this->type = $type;
    }

}