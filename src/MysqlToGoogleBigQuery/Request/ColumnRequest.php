<?php
namespace MysqlToGoogleBigQuery\Request;

class ColumnRequest
{
    public $name = '';
    
    public $type = '';
    
    public $mode = '';
    
    public $description = '';

    /**
     * @var DbColumnRequest|null
     */
    private $dbColumnRequest = null;

    /**
     * @param string $name
     * @param string $type
     * @param string $mode
     * @param string $description
     * @param DbColumnRequest|null $dbColumnRequest
     */
    public function __construct($name, $type, $mode, $description, DbColumnRequest $dbColumnRequest)
    {
        $this->name = $name;
        $this->type = $type;
        $this->mode = $mode;
        $this->description = $description;
        $this->dbColumnRequest = $dbColumnRequest;
    }

    /**
     * @return DbColumnRequest|null
     */
    public function getDbColumnRequest()
    {
        return $this->dbColumnRequest;
    }

}