<?php
namespace MysqlToGoogleBigQuery\Request;

class DbColumnRequest
{
    private $name = '';

    private $type = '';

    private $nullable = '';

    private $lengthMax = 0;

    private $description = '';

    /**
     * DbColumn constructor.
     *
     * @param string $name
     * @param string $type
     * @param string $nullable
     * @param int $lengthMax
     * @param string $description
     */
    public function __construct($name, $type, $nullable, $lengthMax, $description = '')
    {
        $this->name = $name;
        $this->type = $type;
        $this->nullable = $nullable;
        $this->lengthMax = $lengthMax;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getNullable()
    {
        return $this->nullable;
    }

    /**
     * @return int
     */
    public function getLengthMax()
    {
        return $this->lengthMax;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

}