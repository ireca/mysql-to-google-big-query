<?php
namespace MysqlToGoogleBigQuery\Request;

use Doctrine\Common\Collections\ArrayCollection;
use MysqlToGoogleBigQuery\Component\Config\QueryParamConfigList;

class ColumnRequestList
{
    /**
     * @var string
     */
    private $tableName = '';

    /**
     * @var string
     */
    private $dbTableName = '';

    /**
     * @var string
     */
    private $query = '';

    /**
     * @var QueryParamConfigList
     */
    private $queryParamList = array();

    /**
     * @var ArrayCollection
     */
    private $columns = array();

    /**
     * @var string
     */
    private $description = '';

    /**
     * ColumnRequestList constructor.
     *
     * @param string $tableName
     */
    public function __construct($dbTableName, $tableName, $query, QueryParamConfigList $queryParamList, $description)
    {
        $this->dbTableName = $dbTableName;
        $this->tableName = $tableName;
        $this->query = $query;
        $this->queryParamList = $queryParamList;
        $this->description = $description;
        $this->columns = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function getFields() 
    {
        $columns = array();
        /**
         * @var ColumnRequest $columnRequest
         */
        foreach ($this->getColumns()->getValues() as $columnRequest) {
            $columns[] = array(
                'name' => $columnRequest->name,
                'type' => $columnRequest->type,
                'mode' => $columnRequest->mode,
                'description' => $columnRequest->description,
            );
        }

        return $columns;
    }

    /**
     * @return string
     */
    public function getColumnsAsString()
    {
        $queryColumns = array();
        
        /** @var ColumnRequest $column */
        foreach ($this->getColumns()->getValues() as $column) {
            $queryColumns[] = "`{$this->getDbTableName()}`.`{$column->getDbColumnRequest()->getName()}` AS `$column->name` ";
        }
        
        return implode($queryColumns, ',');
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param $tableName
     *
     * @return $this
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
        
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDbTableName()
    {
        return $this->dbTableName;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return QueryParamConfigList
     */
    public function getQueryParamList()
    {
        return $this->queryParamList;
    }

    /**
     * @return ArrayCollection
     */
    public function getColumns()
    {
        return $this->columns;
    }
    
}