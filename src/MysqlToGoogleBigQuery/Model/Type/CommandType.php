<?php
namespace MysqlToGoogleBigQuery\Model\Type;

class CommandType
{
    const CREATE = 'create';
    const UPDATE = 'update';
    const UPDATE_ROWS = 'update_rows';
}