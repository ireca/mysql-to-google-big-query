<?php
namespace MysqlToGoogleBigQuery\Component\Config;

class UpdaterConfig
{
    private $iterationCount = 0;

    private $packageSize = 500;

    private $tables = array();

    /**
     * @param int $iterationCount
     * @param int $packageSize
     * @param array $tables
     */
    public function __construct($iterationCount, $packageSize, array $tables)
    {
        $this->iterationCount = $iterationCount;
        $this->packageSize = $packageSize;
        $this->tables = $tables;
    }

    /**
     * @return int
     */
    public function getIterationCount()
    {
        return $this->iterationCount;
    }

    /**
     * @return int
     */
    public function getPackageSize()
    {
        return $this->packageSize;
    }

    /**
     * @return array
     */
    public function getTableNames()
    {
        return $this->tables;
    }

}