<?php
namespace MysqlToGoogleBigQuery\Component\Config;

use Doctrine\Common\Collections\ArrayCollection;

class QueryParamConfigList extends ArrayCollection
{
    private $table = '';

    /**
     * @param string $table
     */
    public function __construct($table, array $elements = array())
    {
        $this->table = $table;
        parent::__construct($elements);
    }

    /**
     * @return QueryParamConfig[]
     */
    public function getValues()
    {
        return parent::getValues();
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

}