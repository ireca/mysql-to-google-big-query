<?php
namespace MysqlToGoogleBigQuery\Component\Config;

use Doctrine\Common\Collections\ArrayCollection;

class TableConfigList extends ArrayCollection
{
    /**
     * @param $key
     * @return TableConfig|null
     */
    public function get($key)
    {
        return parent::get($key);
    }

    /**
     * @param array $replaceQueryParamLists
     * @return $this
     */
    public function replaceQueryParams(array $replaceQueryParamLists = array())
    {
        foreach ($replaceQueryParamLists as $replaceQueryParamList) {
            /** @var QueryParamConfig $replaceQueryParamConfig */
            foreach ($replaceQueryParamList->getIterator() as $replaceQueryParam) {
                /** @var $tableConfig TableConfig  */
                foreach ($this->getValues() as &$tableConfig) {
                    if ($tableConfig->getName() == $replaceQueryParamList->getTable()) {
                        /** @var $queryParamConfig QueryParamConfig */
                        foreach ($tableConfig->getQueryParamList() as $key => $queryParamConfig) {
                            if ($queryParamConfig->getName() == $replaceQueryParam->getName()) {
                                $tableConfig->getQueryParamList()->set($key, $replaceQueryParam);
                            }
                        }
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getTableNames()
    {
        return $this->getKeys();
    }

    /**
     * @return bool
     */
    public function tableHasSettings($tableName)
    {
        return ! is_null($this->get($tableName));
    }

    /**
     * @param $dbTableName
     * @param $columnName
     * @return string
     */
    public function getColumnName($dbTableName, $columnName)
    {
        $column = $this->get($dbTableName)->getColumnList()->get($columnName);
        if (! is_null($column)) {

            return ! empty($column->getName()) ? $column->getName() : $columnName;
        }

        return $columnName;
    }

    /**
     * @return bool
     */
    public function isHasColumns($dbTableName)
    {
        return (bool) $this->get($dbTableName)->getColumnList()->count();
    }

}