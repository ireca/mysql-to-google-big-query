<?php

namespace MysqlToGoogleBigQuery\Component\Config;

class QueryParamConfig
{
    private $name = '';

    private $value = '';

    private $type = '';

    /**
     * @param string $name
     * @param string|array $value
     * @param string $type
     */
    public function __construct($name, $value, $type)
    {
        $this->name = $name;
        $this->value = $value;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return int|null
     */
    public function getCnqType()
    {
        return null;
    }

}