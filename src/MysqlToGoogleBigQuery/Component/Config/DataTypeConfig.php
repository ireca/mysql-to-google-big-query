<?php

namespace MysqlToGoogleBigQuery\Component\Config;

class DataTypeConfig
{
    private $dbType = '';

    private $storageType = '';

    /**
     * @param string $dbType
     * @param string $storageType
     */
    public function __construct($dbType, $storageType)
    {
        $this->dbType = $dbType;
        $this->storageType = $storageType;
    }

    /**
     * @return string
     */
    public function getDbType()
    {
        return $this->dbType;
    }

    /**
     * @return string
     */
    public function getStorageType()
    {
        return $this->storageType;
    }

}