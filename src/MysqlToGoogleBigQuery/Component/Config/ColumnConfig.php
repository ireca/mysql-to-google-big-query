<?php

namespace MysqlToGoogleBigQuery\Component\Config;

class ColumnConfig
{
    private $column = '';

    private $name = '';

    private $description = '';

    /**
     * @param string $column
     * @param string $name
     * @param string $description
     */
    public function __construct($column, $name, $description = '')
    {
        $this->column = $column;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return mixed|string
     */
    public function getStorageName($default = '')
    {
        return ! empty($this->getName())
            ? $this->getName()
            : $default;
    }

    /**
     * @return mixed|string
     */
    public function getStorageDescription($default = '')
    {
        return ! empty($this->getDescription())
            ? $this->getDescription()
            : $default;
    }

    /**
     * @param $isNullable
     * @return string
     */
    public function getStorageMode($isNullable)
    {
        return ($isNullable == 'YES') ? 'NULLABLE' : 'REQUIRED';
    }

    /**
     * @return string
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

}