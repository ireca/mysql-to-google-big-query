<?php
namespace MysqlToGoogleBigQuery\Component\Config;

use Doctrine\Common\Collections\ArrayCollection;
use MysqlToGoogleBigQuery\Exception\LoggedException;

class DataTypeConfigList extends ArrayCollection
{
    /**
     * @param $key
     * @return DataTypeConfig|null
     */
    public function get($key)
    {
        return parent::get($key);
    }

    /**
     * @param $dbType
     * @return string
     * @throws LoggedException
     */
    public function getStorageDataType($dbType)
    {
        $dataTypeConfig = $this->get(strtoupper($dbType));
        if (is_null($dataTypeConfig)) {
            throw new LoggedException('Unknown MySQL data type:' . $dbType);
        }

        return $dataTypeConfig->getStorageType();
    }

}