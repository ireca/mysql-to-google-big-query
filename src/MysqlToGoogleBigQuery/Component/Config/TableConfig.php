<?php

namespace MysqlToGoogleBigQuery\Component\Config;

class TableConfig
{
    /**
     * @var string
     */
    private $storageTable = '';

    /**
     * @var string
     */
    private $query = '';

    /**
     * @var QueryParamConfigList
     */
    private $queryParams = null;

    /**
     * @var string
     */
    private $table = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var ColumnConfigList
     */
    private $columnList = null;

    /**
     * @param string $storageTable
     * @param string $query
     * @param QueryParamConfigList $queryParams
     * @param string $table
     * @param string $description
     * @param ColumnConfigList|null $columns
     */
    public function __construct($table, $storageTable, $query, QueryParamConfigList $queryParams,
                                ColumnConfigList $columns, $description)
    {
        $this->storageTable = $storageTable;
        $this->query = $query;
        $this->queryParams = $queryParams;
        $this->table = $table;
        $this->description = $description;
        $this->columnList = $columns;
    }

    /**
     * @return mixed|string
     */
    public function getStorageName($default = '')
    {
        return ! empty($this->getStorageTable())
            ? $this->getStorageTable()
            : $default;
    }

    /**
     * @return mixed|string
     */
    public function getStorageDescription($default = '')
    {
        return ! empty($this->getDescription())
            ? $this->getDescription()
            : $default;
    }

    /**
     * @return mixed|string
     */
    public function getStorageQuery($default = '')
    {
        return ! empty($this->getQuery())
            ? $this->getQuery()
            : $default;
    }

    /**
     * @return string
     */
    public function getStorageTable()
    {
        return $this->storageTable;
    }

    /**
     * @param $columnName
     * @return bool
     */
    public function isProcessColumn($columnName)
    {
        //if columns config is empty that create all columns.
        if ($this->getColumnList()->count() == 0) {

            return true;
        }

        if (! is_null($this->getColumnList()->get($columnName))) {

            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return QueryParamConfigList
     */
    public function getQueryParamList()
    {
        return $this->queryParams;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->table;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return ColumnConfigList|null
     */
    public function getColumnList()
    {
        return $this->columnList;
    }

}