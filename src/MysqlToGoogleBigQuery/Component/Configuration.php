<?php
namespace MysqlToGoogleBigQuery\Component;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('mysqlToGoogleBigQuery');

        $rootNode
            ->children()
                ->scalarNode('projectId')
                    ->isRequired()
                ->end()
                ->scalarNode('keyFilePath')
                    ->isRequired()
                ->end()
                ->scalarNode('dataSet')
                    ->isRequired()
                ->end()
                ->arrayNode('databaseConfig')
                    ->children()
                        ->scalarNode('host')
                            ->defaultValue('localhost')
                        ->end()
                        ->scalarNode('port')
                            ->defaultValue('3306')
                        ->end()
                        ->scalarNode('driver')
                            ->defaultValue('pdo_mysql')
                        ->end()
                        ->scalarNode('user')
                            ->isRequired()
                        ->end()
                        ->scalarNode('password')
                            ->isRequired()
                        ->end()
                        ->scalarNode('dbname')
                            ->isRequired()
                        ->end()
                        ->scalarNode('charset')
                            ->defaultValue('UTF8')
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('merger')
                    ->children()
                        ->scalarNode('packageSize')
                            ->defaultValue(1000)
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('updater')
                    ->children()
                        ->scalarNode('iterationCount')
                            ->defaultValue(1)
                        ->end()
                        ->scalarNode('packageSize')
                            ->defaultValue(500)
                        ->end()
                        ->arrayNode('tables')
                            ->defaultValue(array())
                            ->prototype('scalar')
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('fromDataTypeToDataTypes')
                    ->prototype('scalar')
                    ->end()
                ->end()
                ->arrayNode('slackErrorNotifier')
                    ->children()
                        ->arrayNode('errorHandler')->isRequired()
                            ->children()
                                ->scalarNode('server')->isRequired()->end()
                                ->arrayNode('transport')->isRequired()
                                    ->children()
                                        ->scalarNode('type')->isRequired()->end()
                                        ->arrayNode('params')->isRequired()
                                            ->children()
                                                ->scalarNode('hookUrl')
                                                    ->defaultValue('slack')
                                                    ->isRequired()
                                                ->end()
                                                ->scalarNode('botName')->isRequired()->end()
                                                ->scalarNode('chanel')->isRequired()->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('ignoreErrors')
                            ->defaultValue(array())
                            ->prototype('scalar')->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('restLogService')->isRequired()
                    ->children()
                        ->arrayNode('restLog')->isRequired()
                            ->children()
                                ->booleanNode('isActive')->isRequired()->end()
                                ->integerNode('maxCodeCount')->isRequired()->end()
                                ->arrayNode('databaseConfig')->isRequired()
                                    ->children()
                                        ->scalarNode('host')->defaultValue('localhost')->isRequired(false)->end()
                                        ->scalarNode('port')->defaultValue(3306)->isRequired(false)->end()
                                        ->scalarNode('driver')->defaultValue('pdo_mysql')->isRequired(false)->end()
                                        ->scalarNode('user')->isRequired()->end()
                                        ->scalarNode('password')->isRequired()->end()
                                        ->scalarNode('dbname')->isRequired()->end()
                                        ->scalarNode('charset')->defaultValue('UTF8')->isRequired(false)->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('tables')
                    ->defaultValue(array())
                    ->prototype('array')
                        ->children()
                            ->scalarNode('table')
                                ->defaultValue('')
                            ->end()
                            ->scalarNode('query')
                                ->defaultValue('')
                            ->end()
                            ->arrayNode('queryParams')
                                ->defaultValue(array())
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('name')->end()
                                            ->variableNode('value')
                                                ->validate()
                                                    ->ifTrue(function ($value) {
                                                        return false === is_string($value)
                                                            && false === is_array($value)
                                                            && false === is_numeric($value)
                                                            && false === is_float($value);
                                                    })
                                                    ->thenInvalid('The value of "queryParams" can be array or string or number only!')
                                                ->end()
                                            ->end()
                                        ->scalarNode('type')
                                            ->defaultValue('integer')
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->scalarNode('description')
                                ->defaultValue('')
                            ->end()
                            ->arrayNode('columns')
                                ->defaultValue(array())
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('name')
                                            ->defaultValue('')
                                        ->end()
                                        ->scalarNode('description')
                                            ->defaultValue('')
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

}