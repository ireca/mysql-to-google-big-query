<?php
namespace MysqlToGoogleBigQuery\Factory;

use MysqlToGoogleBigQuery\Service\Api\BigQueryApiServiceFactory;
use MysqlToGoogleBigQuery\Service\UpdaterService;
use RestLog\Factory\MessageLogFactory;
use SlackErrorNotifier\Factory\ErrorNotifierFactory;

class UpdaterServiceFactory extends MergerServiceFactory
{
    /**
     * @param $config
     * @param array $queryParamLists
     * @return UpdaterService
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public static function createByConfig($config, array $queryParamLists = array())
    {
        return new UpdaterService(
            ConfigServiceFactory::create(
                $config['mysqlToGoogleBigQuery'],
                $queryParamLists
            ),
            BigQueryApiServiceFactory::create(
                $config['mysqlToGoogleBigQuery']['projectId'],
                $config['mysqlToGoogleBigQuery']['keyFilePath'],
                $config['mysqlToGoogleBigQuery']['dataSet']
            ),
            self::getDatabaseRepository(
                $config['mysqlToGoogleBigQuery']['databaseConfig']
            ),
            self::getJournalProvider(
                $config['mysqlToGoogleBigQuery']['databaseConfig']
            ),
            MessageLogFactory::createByConfig(
                $config['mysqlToGoogleBigQuery']['restLogService']
            ),
            ErrorNotifierFactory::createByConfig(
                $config['mysqlToGoogleBigQuery']['slackErrorNotifier']
            )
        );
    }

}