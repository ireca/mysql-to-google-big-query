<?php
namespace MysqlToGoogleBigQuery\Factory;

use MysqlToGoogleBigQuery\Model\Type\CommandType;
use MysqlToGoogleBigQuery\Provider\JournalProvider;
use MysqlToGoogleBigQuery\Provider\DatabaseProvider;
use MysqlToGoogleBigQuery\Service\MergerService;
use RestLog\Factory\MessageLogFactory;
use SlackErrorNotifier\Factory\ErrorNotifierFactory;
use MysqlToGoogleBigQuery\Service\Api\BigQueryApiServiceFactory;

class MergerServiceFactory extends BaseFactory
{
    /**
     * @param $config
     * @param array $queryParamLists
     * @return MergerService
     * @throws \Exception
     */
    public static function createByConfig($config, array $queryParamLists = array())
    {
        return new MergerService(
            ConfigServiceFactory::create(
                $config['mysqlToGoogleBigQuery'],
                $queryParamLists
            ),
            BigQueryApiServiceFactory::create(
                $config['mysqlToGoogleBigQuery']['projectId'],
                $config['mysqlToGoogleBigQuery']['keyFilePath'],
                $config['mysqlToGoogleBigQuery']['dataSet']
            ),
            self::getDatabaseRepository(
                $config['mysqlToGoogleBigQuery']['databaseConfig']
            ),
            self::getJournalProvider(
                $config['mysqlToGoogleBigQuery']['databaseConfig']
            ),
            MessageLogFactory::createByConfig(
                $config['mysqlToGoogleBigQuery']['restLogService']
            ),
            ErrorNotifierFactory::createByConfig(
                $config['mysqlToGoogleBigQuery']['slackErrorNotifier']
            )
        );
    }



    /**
     * @param $databaseConfig
     *
     * @return DatabaseProvider|null
     */
    protected static function getDatabaseRepository($databaseConfig)
    {
        if (count($databaseConfig) == 0) {

            return null;
        }

        return new DatabaseProvider($databaseConfig);
    }

    /**
     * @param $databaseConfig
     * @return JournalProvider|null
     */
    protected static function getJournalProvider($databaseConfig)
    {
        if (count($databaseConfig) == 0) {

            return null;
        }

        return new JournalProvider($databaseConfig);
    }    

}