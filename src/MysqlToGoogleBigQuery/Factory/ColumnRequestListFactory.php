<?php
namespace MysqlToGoogleBigQuery\Factory;

use MysqlToGoogleBigQuery\Component\Config\TableConfig;
use MysqlToGoogleBigQuery\Request\ColumnRequestList;

class ColumnRequestListFactory extends BaseFactory
{
    /**
     * @param $dbTableName
     * @param TableConfig $tableConfig
     * @return ColumnRequestList
     */
    public static function create($dbTableName, TableConfig $tableConfig)
    {
        return new ColumnRequestList(
            $dbTableName,
            $tableConfig->getStorageName($dbTableName),
            $tableConfig->getQuery(),
            $tableConfig->getQueryParamList(),
            $tableConfig->getStorageDescription()
        );
    }

}