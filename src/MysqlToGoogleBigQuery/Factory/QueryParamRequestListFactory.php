<?php
namespace MysqlToGoogleBigQuery\Factory;

use MysqlToGoogleBigQuery\Component\Config\QueryParamConfigList;
use MysqlToGoogleBigQuery\Request\QueryParamRequest;
use MysqlToGoogleBigQuery\Request\QueryParamRequestList;

class QueryParamRequestListFactory extends BaseFactory
{
    /**
     * @param QueryParamConfigList $queryParams
     * @return QueryParamRequestList
     */
    public static function create(QueryParamConfigList $queryParams)
    {
        $items = array();
        foreach ($queryParams as $queryParam) {
            $items[] = new QueryParamRequest(
                $queryParam['name'],
                $queryParam['value'],
                $queryParam['type']
            );
        }

        return new QueryParamRequestList($items);
    }
}