<?php
namespace MysqlToGoogleBigQuery\Service;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use MysqlToGoogleBigQuery\Exception\LoggedException;
use MysqlToGoogleBigQuery\Model\Type\CommandType;
use MysqlToGoogleBigQuery\Provider\Merger\MergerProviderFactory;
use MysqlToGoogleBigQuery\Service\Result\StorageTable;
use MysqlToGoogleBigQuery\Service\Result\StorageTableList;

class MergerService extends BaseCommandService
{
    /**
     * @return bool
     * @throws LoggedException
     * @throws DBALException
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function run($commandPostfix = '', $description = '')
    {
        $columnRequestLists = $this->getColumnRequests(
            $this->getConfigService()->getTableConfigList()->getTableNames(),
            $this->getConfigService()->getTableConfigList(),
            $this->getConfigService()->getDataTypeConfigList()
        );
        $storageTableList = $this->createTablesInStorage(
            $columnRequestLists,
            $this->getConfigService()->getMerger()->getPackageSize()
        );
        $this->copyDataToStorageTables(
            $this->getConfigService()->getTableConfigList()->getTableNames(),
            $storageTableList,
            $commandPostfix,
            $description
        );

        return true;
    }

    /**
     * @param StorageTableList $storageTableList)
     * @return true
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function copyDataToStorageTables(array $dbTableNames, StorageTableList $storageTableList, $commandPostfix,
                                               $description)
    {
        $customCreateCommand = $this->getCommandType(CommandType::CREATE, $commandPostfix);
        $journalItemResult = $this
            ->getJournalProvider()
            ->getLastJournalItems(array(CommandType::CREATE, $customCreateCommand), $dbTableNames);
        foreach ($storageTableList->getValues() as $storageTable) {
            $this->copyData(
                $storageTable,
                $journalItemResult->getLastId($storageTable->getColumnRequestList()->getDbTableName()),
                ($storageTable->getColumnRequestList()->getQueryParamList()->count() > 0)
                    ? $customCreateCommand
                    : CommandType::CREATE,
                ($storageTable->getColumnRequestList()->getQueryParamList()->count() > 0)
                    ? $description
                    : ''
            );
        }

        return true;
    }

    /**
     * @param StorageTable $storageTable
     * @param $lastId
     * @param $commandType
     * @param $description
     * @return bool
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function copyData(StorageTable $storageTable, $lastId, $commandType, $description)
    {
        $entityProvider = MergerProviderFactory::create(
            $this->getDatabaseRepository()->getEm(),
            $storageTable->getColumnRequestList()
        );
        try {
            $iterationResult = $entityProvider->getIterationRowCount($lastId);
            for ($i = 0; $i < $iterationResult->getRowCount(); $i += $storageTable->getPackageSize()) {
                $rowList = $entityProvider->getRows($lastId, $storageTable->getPackageSize());
                if (count($rowList->getValues()) == 0) {

                    continue;
                }
                if ($this->getBigQueryApiService()->multipleInsert($storageTable->getTable(), $rowList->getData()) == false) {
                    throw new LoggedException($this->getBigQueryApiService()->getErrorMessage());
                }
                $lastId = $rowList->getValueLastId();
            }
            $this
                ->getJournalProvider()
                ->create(
                    $commandType,
                    $storageTable->getColumnRequestList()->getDbTableName(),
                    $iterationResult->getMinId(),
                    $iterationResult->getMaxId(),
                    $iterationResult->getRowCount(),
                    $description
                );
        } catch (\Exception $ex) {
            $this->getJournalProvider()->create(
                $commandType,
                $storageTable->getColumnRequestList()->getDbTableName(),
                isset($iterationResult) ? $iterationResult->getMinId() : 0,
                $lastId,
                isset($i) ? $i : 0,
                $description,
                false,
                $ex->getMessage()
            );
            $this->writeToLogAndSendNotify(
                $storageTable->getColumnRequestList()->getDbTableName(),
                $lastId,
                isset($i) ? $i : 0,
                $ex->getMessage()
            );

            return false;
        }

        return true;
    }

}