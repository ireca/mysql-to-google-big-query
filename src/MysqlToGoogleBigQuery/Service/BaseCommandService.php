<?php
namespace MysqlToGoogleBigQuery\Service;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use MysqlToGoogleBigQuery\Component\Config\ColumnConfig;
use MysqlToGoogleBigQuery\Component\Config\DataTypeConfigList;
use MysqlToGoogleBigQuery\Component\Config\TableConfig;
use MysqlToGoogleBigQuery\Component\Config\TableConfigList;
use MysqlToGoogleBigQuery\Exception\LoggedException;
use MysqlToGoogleBigQuery\Factory\ColumnRequestFactory;
use MysqlToGoogleBigQuery\Factory\ColumnRequestListFactory;
use MysqlToGoogleBigQuery\Provider\JournalProvider;
use MysqlToGoogleBigQuery\Provider\DatabaseProvider;
use MysqlToGoogleBigQuery\Request\ColumnRequestList;
use MysqlToGoogleBigQuery\Service\Api\BigQueryApiService;
use MysqlToGoogleBigQuery\Service\Result\StorageTable;
use MysqlToGoogleBigQuery\Service\Result\StorageTableList;
use RestLog\Service\Transport\MessageLogService;
use SlackErrorNotifier\Service\Transport\NotifierService;

abstract class BaseCommandService extends BaseService
{
    /**
     * @var ConfigService|null
     */
    private $configService = null;

    /**
     * @var BigQueryApiService|null
     */
    private $bigQueryApiService = null;

    /**
     * @var DatabaseProvider|null
     */
    private $databaseRepository = null;

    /**
     * @var JournalProvider|null
     */
    private $journalProvider = null;

    /**
     * @var MessageLogService
     */
    private $logService = null;

    /**
     * @var NotifierService
     */
    private $notifyService = null;

    /**
     * MergerService constructor.
     *
     * @param ConfigService $configService
     * @param BigQueryApiService $bigQueryApiService
     * @param DatabaseProvider $databaseRepository
     */
    public function __construct(ConfigService     $configService, BigQueryApiService $bigQueryApiService,
                                DatabaseProvider  $databaseRepository, JournalProvider $journalProvider,
                                MessageLogService $logService = null, NotifierService $notifyService = null)
    {
        $this->configService = $configService;
        $this->bigQueryApiService = $bigQueryApiService;
        $this->databaseRepository = $databaseRepository;
        $this->journalProvider = $journalProvider;
        $this->logService = $logService;
        $this->notifyService = $notifyService;
    }

    /**
     * @return bool
     * @throws LoggedException
     * @throws DBALException
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    abstract public function run($commandPostfix = '', $description = '');

    /**
     * @return ColumnRequestList[]
     *
     * @throws DBALException|ORMException|LoggedException
     */
    protected function getColumnRequests(array $dbTableNames, TableConfigList $tableConfigList, DataTypeConfigList $dataTypeList)
    {
        $oldTableName = '';
        $columnRequestLists = array();
        $columns = $this->getDatabaseRepository()->getSchemaInfo($dbTableNames);
        foreach ($columns as $column) {
            if ($column['TABLE_NAME'] != $oldTableName) {
                /** @var TableConfig $tableConfig */
                $tableConfig = $tableConfigList->get($column['TABLE_NAME']);
                if (is_null($tableConfig)) {
                    throw new LoggedException("The table doesn't have configuration settings!");
                }
                $columnRequestList = ColumnRequestListFactory::create($column['TABLE_NAME'], $tableConfig);
                $columnRequestLists[] = $columnRequestList;
            }

            if (! isset($columnRequestList)) {
                throw new LoggedException("The column request list wasn't initialized!");
            }

            if (! isset($tableConfig)) {
                throw new LoggedException("The table config wasn't initialized!");
            }

            if ($tableConfig->isProcessColumn($column['COLUMN_NAME'])) {
                $columnConfig = $tableConfig->getColumnList()->get($column['COLUMN_NAME']);
                if (is_null($columnConfig)) {
                    $columnConfig = new ColumnConfig($column['COLUMN_NAME'], $column['COLUMN_NAME']);
                }
                $columnRequestList->getColumns()->add(
                    ColumnRequestFactory::create($column, $columnConfig, $dataTypeList)
                );
            }
            $oldTableName = $column['TABLE_NAME'];
        }

        return $columnRequestLists;
    }

    /**
     * @param array $columnRequestLists
     * @param $packageSize
     * @return StorageTableList
     */
    protected function createTablesInStorage(array $columnRequestLists, $packageSize)
    {
        $storageTableList = new StorageTableList();
        /** @var ColumnRequestList $columnRequestList */
        foreach ($columnRequestLists as $columnRequestList) {
            $table = $this->getBigQueryApiService()->createTable(
                $columnRequestList->getTableName(),
                $columnRequestList->getFields(),
                $columnRequestList->getDescription()
            );
            $storageTableList->add(new StorageTable($table, $columnRequestList, $packageSize));
        }

        return $storageTableList;
    }

    /**
     * @param $commandType
     * @param $postfix
     * @return string
     */
    protected function getCommandType($commandType, $postfix)
    {
        if (empty($postfix)) {

            return $commandType;
        }

        return $commandType . '_' . $postfix;
    }

    /**
     * @param $table
     * @param $lastId
     * @param $itemCount
     * @param $message
     * @return bool
     */
    protected function writeToLogAndSendNotify($table, $lastId, $itemCount, $message)
    {
        $errorMessage = sprintf("The MySQLtoGBQ service couldn't send data of the '%s' table from ID:".
            " '%s', to ID: %s. Got the error message: %s",
            $table,
            $lastId,
            $itemCount,
            $message
        );
        $isWrite = $this->writeToLog($errorMessage);
        $isSend = $this->sendNotify($errorMessage);

        return $isWrite && $isSend;
    }

    /**
     * @param $errorMessage
     * @return bool
     */
    protected function writeToLog($errorMessage)
    {
        $isWrite = false;
        $logService = $this->getLogService();
        if (! is_null($logService)) {
            try {
                $logService->error($errorMessage);
                $isWrite = true;
            } catch (\Exception $ex) {
                //if an error message wasn't write to the log it isn't problem for stop cycle.
            }
        }

        return $isWrite;
    }

    /**
     * @param $errorMessage
     * @return true
     */
    protected function sendNotify($errorMessage)
    {
        $isSend = true;
        $notifyService = $this->getNotifyService();
        if (! is_null($notifyService)) {
            $isSend = $notifyService->getApiService()->sendNotify($errorMessage);
        }

        return $isSend;
    }

    /**
     * @return ConfigService|null
     */
    protected function getConfigService()
    {
        return $this->configService;
    }

    /**
     * @return BigQueryApiService|null
     */
    protected function getBigQueryApiService()
    {
        return $this->bigQueryApiService;
    }

    /**
     * @return DatabaseProvider|null
     */
    protected function getDatabaseRepository()
    {
        return $this->databaseRepository;
    }

    /**
     * @return JournalProvider|null
     */
    protected function getJournalProvider()
    {
        return $this->journalProvider;
    }

    /**
     * @return MessageLogService|null
     */
    protected function getLogService()
    {
        return $this->logService;
    }

    /**
     * @return NotifierService|null
     */
    protected function getNotifyService()
    {
        return $this->notifyService;
    }

}