<?php
namespace MysqlToGoogleBigQuery\Service;

use MysqlToGoogleBigQuery\Service\Api\ApiServiceInterface;

class StorageService extends BaseService
{
    /**
     * @var ApiServiceInterface
     */
    private $api = null;

    /**
     * @param ApiServiceInterface|null $api
     */
    public function __construct(ApiServiceInterface $api)
    {
        $this->api = $api;
    }

    /**
     * @param $storageTableName
     * @param array $attributes
     * @param $id
     * @return array
     * @throws \MysqlToGoogleBigQuery\Exception\LoggedException
     */
    public function update($storageTableName, array $attributes, $id)
    {
        return $this->getApi()->update($storageTableName, $attributes, $id);
    }

    /**
     * @return ApiServiceInterface|null
     */
    public function getApi()
    {
        return $this->api;
    }

}