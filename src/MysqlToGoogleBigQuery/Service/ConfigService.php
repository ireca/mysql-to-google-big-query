<?php
namespace MysqlToGoogleBigQuery\Service;

use MysqlToGoogleBigQuery\Component\Config\DataTypeConfig;
use MysqlToGoogleBigQuery\Component\Config\DataTypeConfigList;
use MysqlToGoogleBigQuery\Component\Config\MergerConfig;
use MysqlToGoogleBigQuery\Component\Config\QueryParamConfig;
use MysqlToGoogleBigQuery\Component\Config\QueryParamConfigList;
use MysqlToGoogleBigQuery\Component\Config\ColumnConfig;
use MysqlToGoogleBigQuery\Component\Config\ColumnConfigList;
use MysqlToGoogleBigQuery\Component\Config\TableConfig;
use MysqlToGoogleBigQuery\Component\Config\TableConfigList;
use MysqlToGoogleBigQuery\Component\Config\UpdaterConfig;
use MysqlToGoogleBigQuery\Component\Configuration;
use MysqlToGoogleBigQuery\Exception\LoggedException;
use Symfony\Component\Config\Definition\Processor;

class ConfigService extends BaseService
{
    /**
     * @var array
     */
    private $config = array();

    /**
     * @var \MysqlToGoogleBigQuery\Component\Configuration|null
     */
    private $configuration = null;

    /**
     * @var string
     */
    private $dataSet = '';

    /**
     * @var DataTypeConfigList
     */
    private $dataTypeConfigList = null;

    /**
     * @var TableConfigList
     */
    private $tableConfigList = null;

    /**
     * @var MergerConfig
     */
    private $merger = null;

    /**
     * @var UpdaterConfig
     */
    private $updater = null;

    /**
     * @var array
     */
    private $notifier = array();

    /**
     * @var array
     */
    private $logService = array();

    /**
     * ConfigService constructor.
     *
     * @param \MysqlToGoogleBigQuery\Component\Configuration|null $configuration
     */
    public function __construct(\MysqlToGoogleBigQuery\Component\Configuration $configuration, $config)
    {
        $this->configuration = $configuration;
        $this->config = $config;
    }

    /**
     * @param QueryParamConfigList[] $queryParamLists
     * @return $this
     * @throws \Exception
     */
    public function loadConfig(array $queryParamLists = array())
    {
        $this->dataSet = $this->getConfig()['dataSet'];
        $this->dataTypeConfigList = $this->loadDateTypes(
            $this->getConfig()['fromDataTypeToDataTypes']
        );
        $this->merger = new MergerConfig(
            $this->getConfig()['merger']['packageSize']
        );
        $this->updater = new UpdaterConfig(
            $this->getConfig()['updater']['iterationCount'] - 1,
            $this->getConfig()['updater']['packageSize'],
            $this->getConfig()['updater']['tables']
        );
        $this->notifier = $this->getConfig()['slackErrorNotifier'];
        $this->logService = $this->getConfig()['restLogService'];
        $this->tableConfigList = $this
            ->loadTableConfigList($this->getConfig()['tables'])
            ->replaceQueryParams($queryParamLists);

        
        return $this;
    }

    /**
     * @param array $dateTypes
     * @return DataTypeConfigList
     */
    private function loadDateTypes(array $dateTypes)
    {
        $list = new DataTypeConfigList();
        foreach ($dateTypes as $dbType => $storageType) {
            $list->set($dbType, new DataTypeConfig($dbType, $storageType));
        }

        return $list;
    }

    /**
     * @return TableConfigList|null
     */
    public function getTableConfigList()
    {
        return $this->tableConfigList;
    }

    /**
     * @param array $tables
     * @return TableConfigList
     */
    private function loadTableConfigList(array $tables)
    {
        $list = new TableConfigList();
        foreach ($tables as $tableName => $tableConfig) {
            $columnList = new ColumnConfigList();
            foreach ($tableConfig['columns'] as $columnName => $columnConfig) {
                $columnList->set(
                    $columnName,
                    new ColumnConfig(
                        $columnName,
                        $columnConfig['name'],
                        $columnConfig['description']
                    )
                );
            }
            $queryParamList = new QueryParamConfigList($tableName);
            foreach ($tableConfig['queryParams'] as $queryParamConfig) {
                $queryParamList->set(
                    $queryParamConfig['name'],
                    new QueryParamConfig(
                        $queryParamConfig['name'],
                        $queryParamConfig['value'],
                        $queryParamConfig['type']
                    )
                );
            }
            $list->set(
                $tableName,
                new TableConfig(
                    $tableName,
                    $tableConfig['table'],
                    $tableConfig['query'],
                    $queryParamList,
                    $columnList,
                    $tableConfig['description']
            ));
        }

        return $list;
    }

    /**
     * @return bool
     */
    public function isValidate()
    {
        $processor = new Processor();
        try {
            $this->setConfig(
                $processor->processConfiguration(new Configuration(), $this->config)
            );
        } catch (\Exception $ex) {
            $this->setErrorMessage($ex->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getDataSet()
    {
        return $this->dataSet;
    }

    /**
     * @param $dataType
     *
     * @return mixed
     * @throws LoggedException
     */
    public function getDataType($dataType)
    {
        $dataType = strtoupper($dataType);
        $dataTypes = $this->getDataTypeConfigList();
        if (! isset($dataTypes[$dataType])) {
            throw new LoggedException('Unknown MySQL data type:' . $dataType);
        }

        return $dataTypes[$dataType];
    }

    /**
     * @return mixed
     */
    public function getPackageSize()
    {
        return $this->merger['packageSize'];
    }

    /**
     * @return int
     */
    public function getIterationCount()
    {
        return $this->updater['iterationCount'];
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return DataTypeConfigList
     */
    public function getDataTypeConfigList()
    {
        return $this->dataTypeConfigList;
    }

    /**
     * @return UpdaterConfig|null
     */
    public function getUpdater()
    {
        return $this->updater;
    }

    /**
     * @return MergerConfig|null
     */
    public function getMerger()
    {
        return $this->merger;
    }

}