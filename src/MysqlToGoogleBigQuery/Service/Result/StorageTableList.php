<?php
namespace MysqlToGoogleBigQuery\Service\Result;

use Doctrine\Common\Collections\ArrayCollection;

class StorageTableList extends ArrayCollection
{
    /**
     * @param $key
     * @return StorageTable|null
     */
    public function get($key)
    {
        return parent::get($key);
    }

    /**
     * @return StorageTable[]
     */
    public function getValues()
    {
        return parent::getValues();
    }

}