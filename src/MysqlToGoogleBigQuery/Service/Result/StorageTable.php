<?php
namespace MysqlToGoogleBigQuery\Service\Result;

use Google\Cloud\BigQuery\Table;
use MysqlToGoogleBigQuery\Request\ColumnRequestList;

class StorageTable
{
    /**
     * @var Table
     */
    private $table = null;

    /**
     * @var ColumnRequestList
     */
    private $columnRequestList = null;

    /**
     * @var int
     */
    private $packageSize = 0;

    /**
     * @param Table|null $table
     * @param ColumnRequestList|null $columnRequestList
     * @param int $packageSize
     */
    public function __construct(Table $table, ColumnRequestList $columnRequestList, $packageSize)
    {
        $this->table = $table;
        $this->columnRequestList = $columnRequestList;
        $this->packageSize = $packageSize;
    }

    /**
     * @return Table|null
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @return ColumnRequestList|null
     */
    public function getColumnRequestList()
    {
        return $this->columnRequestList;
    }

    /**
     * @return int
     */
    public function getPackageSize()
    {
        return $this->packageSize;
    }

}