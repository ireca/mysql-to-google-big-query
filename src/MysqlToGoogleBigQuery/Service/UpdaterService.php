<?php
namespace MysqlToGoogleBigQuery\Service;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use MysqlToGoogleBigQuery\Exception\LoggedException;
use MysqlToGoogleBigQuery\Model\Type\CommandType;
use MysqlToGoogleBigQuery\Provider\Updater\UpdaterEntityProviderFactory;
use MysqlToGoogleBigQuery\Service\Result\StorageTable;
use MysqlToGoogleBigQuery\Service\Result\StorageTableList;

class UpdaterService extends BaseCommandService
{
    /**
     * @return bool
     * @throws LoggedException
     * @throws DBALException
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function run($commandPostfix = '', $description = '')
    {
        $columnRequestLists = $this->getColumnRequests(
            $this->getConfigService()->getUpdater()->getTableNames(),
            $this->getConfigService()->getTableConfigList(),
            $this->getConfigService()->getDataTypeConfigList()
        );
        $storageTableList = $this->createTablesInStorage(
            $columnRequestLists,
            $this->getConfigService()->getUpdater()->getPackageSize()
        );
        $this->copyDataToStorageTables(
            $this->getConfigService()->getUpdater()->getTableNames(),
            $storageTableList,
            $commandPostfix,
            $description
        );

        return true;
    }

    /**
     * @param StorageTableList $storageTableList)
     * @return true
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function copyDataToStorageTables(array $dbTableNames, StorageTableList $storageTableList, $commandPostfix,
                                               $description)
    {
        $customCreateCommand = $this->getCommandType(CommandType::CREATE, $commandPostfix);
        $customUpdateCommand = $this->getCommandType(CommandType::UPDATE, $commandPostfix);
        for ($i = $this->getConfigService()->getUpdater()->getIterationCount(); $i >= 0; $i--) {
            $journalItemResult =
                $this->getJournalProvider()
                    ->getItemsByIteration($dbTableNames, array_unique(array(CommandType::CREATE, $customCreateCommand)), $i);
            foreach ($storageTableList->getValues() as $storageTable) {
                $isCustomQuery = ($storageTable->getColumnRequestList()->getQueryParamList()->count() > 0);
                $this->copyData(
                    $storageTable,
                    $journalItemResult->getFirstId($storageTable->getColumnRequestList()->getDbTableName()),
                    $journalItemResult->getLastId($storageTable->getColumnRequestList()->getDbTableName()),
                    $isCustomQuery ? $customUpdateCommand : CommandType::UPDATE,
                    $isCustomQuery ? $description : ''
                );
            }
        }

        return true;
    }

    /***
     * @param StorageTable $storageTable
     * @param $firstId
     * @param $lastId
     * @param $commandType
     * @param $description
     * @return bool
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function copyData(StorageTable $storageTable, $firstId, $lastId, $commandType, $description)
    {
        $iterationFirstId = $firstId;
        $iterationLastId = $lastId;
        $entityProvider = UpdaterEntityProviderFactory::create(
            $this->getDatabaseRepository()->getEm(),
            $storageTable->getColumnRequestList(),
            $this->getBigQueryApiService()
        );
        try {
            $iterationSize = $entityProvider->getIterationRowCount($firstId, $lastId);
            for ($i = 0; $i < $iterationSize; $i += $storageTable->getPackageSize()) {
                $rowList = $entityProvider->getRows($firstId, $lastId);
                if (count($rowList->getValues()) == 0) {

                    continue;
                }
                if ($this->getBigQueryApiService()->multipleInsert($storageTable->getTable(), $rowList->getData()) == false) {
                    throw new LoggedException($this->getBigQueryApiService()->getErrorMessage());
                }
                $lastId = $rowList->getValueLastId();
            }
            if ($iterationSize > 0) {
                $this
                    ->getJournalProvider()
                    ->create(
                        $commandType,
                        $storageTable->getColumnRequestList()->getDbTableName(),
                        $iterationFirstId,
                        $iterationLastId,
                        $iterationSize,
                        $description
                    );
            }
        } catch (\Exception $ex) {
            $this->getJournalProvider()->create(
                $commandType,
                $storageTable->getColumnRequestList()->getDbTableName(),
                $iterationFirstId,
                $iterationLastId,
                isset($i) ? $i : 0,
                $description,
                false,
                $ex->getMessage()
            );
            $this->writeToLogAndSendNotify(
                $storageTable->getColumnRequestList()->getDbTableName(),
                $lastId,
                isset($i) ? $i : 0,
                $ex->getMessage()
            );

            return false;
        }

        return true;
    }

}