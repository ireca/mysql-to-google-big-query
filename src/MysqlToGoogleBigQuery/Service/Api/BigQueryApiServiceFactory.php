<?php
namespace MysqlToGoogleBigQuery\Service\Api;

use Google\Cloud\BigQuery\BigQueryClient;

class BigQueryApiServiceFactory
{
    /**
     * @param $projectId
     * @param $keyFilePath
     * @param $bigQueryDataSet
     * @return BigQueryApiService
     */
    public static function create($projectId, $keyFilePath, $bigQueryDataSet)
    {
        $f = fopen($keyFilePath, 'r');
        $keyFileValue = fread($f, filesize($keyFilePath));
        fclose($f);

        $bigQueryClient = new BigQueryClient(array(
            'projectId' => $projectId,
            'keyFile' => json_decode($keyFileValue, true))
        );

        return new BigQueryApiService($projectId, $bigQueryDataSet, $bigQueryClient);
    }
}