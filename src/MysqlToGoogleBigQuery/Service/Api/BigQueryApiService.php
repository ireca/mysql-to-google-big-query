<?php
namespace MysqlToGoogleBigQuery\Service\Api;

use Google\Cloud\BigQuery\BigQueryClient;
use Google\Cloud\BigQuery\Table;
use MysqlToGoogleBigQuery\Exception\LoggedException;
use MysqlToGoogleBigQuery\Service\BaseService;

class BigQueryApiService extends BaseService implements ApiServiceInterface
{
    /**
     * @var string
     */
    private $projectId = '';

    /**
     * @var string
     */
    private $bigQueryDataSet = '';

    /**
     * @var BigQueryClient|null
     */
    private $bigQueryClient = null;

    /**
     * BigQueryApiService constructor.
     *
     * @param string $projectId
     */
    public function __construct($projectId, $bigQueryDataSet, BigQueryClient $bigQueryClient)
    {
        $this->projectId = $projectId;
        $this->bigQueryDataSet = $bigQueryDataSet;
        $this->bigQueryClient = $bigQueryClient;
    }

    /**
     * @param $bigQueryTable
     * @param array $columns
     * @param $description
     * @return Table
     */
    public function createTable($bigQueryTable, array $columns, $description)
    {
        $dataset = $this
            ->getBigQueryClient()
            ->dataset($this->bigQueryDataSet, $this->projectId);
        if ($dataset->table($bigQueryTable)->exists() == true) {

            return $dataset->table($bigQueryTable);
        }

        $table = $dataset->createTable(
            $bigQueryTable,
            ['schema' => [
                'fields' => $columns],
                'description' => $description
            ]
        );

       return $table;
    }

    /**
     * @param Table $table
     * @param array $data
     *
     * @return bool
     */
    public function multipleInsert(Table $table, array $data)
    {
        try {
            $insertResponse = $table->insertRows($data);
        } catch (\Exception $ex) {
            $this->setErrorMessage($ex->getMessage());
            
            return false;
        }
        
        if ($insertResponse->isSuccessful() == false) {
            $errorMessage = '';
            foreach ($insertResponse->failedRows() as $row) {
                foreach ($row['errors'] as $error) {
                    $errorMessage .= $error['reason'] . ':' . $error['message'] . ';' . "\r\n";
                }
            }
            $this->setErrorMessage($errorMessage);

            return false;
        }

        return true;
    }

    /**
     * @param $storageTableName
     * @param $fromId
     * @param $toId
     * @return array
     * @throws LoggedException
     */
    public function getRowIds($storageTableName, $fromId, $toId)
    {
        $sql = sprintf(
            'SELECT `id` FROM `%s`.`%s` WHERE `id`>=%s AND `id`<=%s',
            $this->bigQueryDataSet,
            $storageTableName,
            $fromId,
            $toId
        );
        $queryJobConfig = $this->getBigQueryClient()->query($sql);
        try {
            $queryResults = $this
                ->getBigQueryClient()
                ->runQuery($queryJobConfig);
        } catch (\Exception $ex) {
            throw new LoggedException($ex->getMessage());
        }

        $ids = array();
        foreach ($queryResults as $result) {
            $ids[] = $result['id'];
        }

        return $ids;
    }

    /**
     * @param $storageTableName
     * @param array $attributes
     * @param $id
     * @return bool
     * @throws LoggedException
     */
    public function update($storageTableName, array $attributes, $id)
    {
        if (empty($id)) {
            throw new LoggedException("id can't be empty!");
        }

        if (count($attributes) == 0) {
            throw new LoggedException("attributes can't be empty!");
        }

        $values = array();
        foreach ($attributes as $name => $value) {
            $values[] = "$name=@$name";
        }
        $setExpression = implode(', ', $values);

        $sql = sprintf(
            'UPDATE `%s`.`%s` SET %s WHERE `id`=%s',
            $this->bigQueryDataSet,
            $storageTableName,
            $setExpression,
            $id
        );
        $queryJobConfig = $this
            ->getBigQueryClient()
            ->query($sql)
            ->parameters($attributes);
        try {
            $this
                ->getBigQueryClient()
                ->runQuery($queryJobConfig);
        } catch (\Exception $ex) {
            throw new LoggedException($ex->getMessage());
        }

        return true;
    }

    /**
     * @return BigQueryClient|null
     */
    public function getBigQueryClient()
    {
        return $this->bigQueryClient;
    }

}