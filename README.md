# MySql to Google BigQuery - Copies data from MySQL server to [Google BigQuery]((https://cloud.google.com/bigquery/docs))

MySql to Google BigQuery - is a library that allows you to copy data from MySQL database to [Google BigQuery](https://developers.google.com/maps/documentation/geocoding/start?hl=ru).
All data will be copied on first launch. On subsequent launches, only new data.

## Installation

Install the latest version with

```bash
$ composer require ireca/mysql-to-google-big-query
$ php vendor/doctrine/migrations/bin/doctrine-migrations.php migrations:migrate --configuration config/migrations-config.yml --db-configuration config/database-config.php
```

## Basic Usage

```php
<?php

use MysqlToGoogleBigQuery\Factory\MergerServiceFactory;
 use MysqlToGoogleBigQuery\Factory\UpdaterServiceFactory

$config = array(
    'mysqlToGoogleBigQuery' => array(
        'projectId' => '',
        'keyFilePath' => '',
        'dataSet' => 'bigQueryDataSet',
        'databaseConfig' => array(
            'driver'   => 'pdo_mysql',
            'user'     => 'root',
            'password' => '',
            'dbname'   => 'database',
            'charset' => 'UTF8',
        ),
        'merger' => array(
            'packageSize' => 1000
        ),
        'updater' => array(
          'packageSize' => 100,
          // Every run copy command create iteration record in the journal table.
          // So this parameter allow copy rows some iteration before.
          'iterationCount' => 1,
          //Only these tables can be use update command.
          'tables' => array('tb_clients')
        ),        
        'fromDataTypeToDataTypes' => array(
            'TINYINT' => 'INT64',
            'SMALLINT' => 'INT64',
            'MEDIUMINT' => 'INT64',
            'INT' => 'INT64',
            'BIGINT' => 'INT64',
            'DECIMAL' => 'BIGNUMERIC',
            'FLOAT' => 'FLOAT64',
            'DOUBLE' => 'FLOAT64',
            'BIT' => 'BOOL',
            'CHAR' => 'STRING',
            'VARCHAR' => 'STRING',
            'BINARY' => 'BYTES',
            'VARBINARY' => 'BYTES',
            'TINYTEXT' => 'STRING',
            'TEXT' => 'STRING',
            'MEDIUMTEXT' => 'STRING',
            'LONGTEXT' => 'STRING',
            'ENUM' => 'STRING',  //No type for ENUM.Must use any type which can represent values in ENUM
            'SET' => 'STRING',  //No type for SET.Must use any type which can represent values in SET
            'DATE' => 'DATE',
            'TIME' => 'TIME',
            'DATETIME' => 'DATETIME',
            'TIMESTAMP' => 'TIMESTAMP'
        ),
        'tables' => array(
            'tg_clients' => array(
                'description' => 'Client table',
                'columns' => array(
                    'id' => array(
                        'description' => ''
                    ),
                    'companyId' => array(
                        'description' => 'Company ID '
                    ),
                    'name' => array(
                        'name' => 'title',
                        'description' => 'Client name'
                    ),
                )
            ),
            'tg_orders' => array(
                // Don't use "IN" expression only string because it vendor use old version doctrine with some bugs.
                'query' => 'SELECT * FROM `tg_orders` WHERE companyId=:companyId AND FIND_IN_SET(`status`, :statuses) > 0',
                'queryParams' => array(
                    array(
                        'name' => 'companyId',
                        'value' => 134,
                        'type' => \Doctrine\DBAL\Types\Type::INTEGER
                    ),
                    array(
                        'name' => 'statuses',
                        'value' => '9,10,11',
                        'type' => \Doctrine\DBAL\Types\Type::STRING
                    ),
                ),            
                'description' => 'Order table',
                'columns' => array(
                    'id' => array(
                        'description' => 'id'
                    ),
                    'clientId' => array(
                        'description' => 'Client id'
                    ),                    
                    'title' => array(
                        'description' => 'Order name'
                    ),
                    'status' => array(
                        'description' => 'Status of an order'
                    ),                    
                )
            ),
            'tg_order_statuses' => array(
                'description' => 'The status types of th order table'
            ),                        
        ),
    )
);

// copies all data to GBQ.
$mergeService = MergerServiceFactory::createByConfig($config);
$mergeService->run();

// copies order rows for all companies that have 1, 2 and 3 statuses only to GBQ.
$qpList = new QueryParamConfigList('tg_orders');
$qpList->add(new QueryParamConfig('companyId', '567', \Doctrine\DBAL\Types\Type::INTEGER));
$qpList->add(new QueryParamConfig('statuses', '1,2,3', \Doctrine\DBAL\Types\Type::STRING));
$mergeService = MergerServiceFactory::createByConfig($config, $qpList);
$mergeService->run();

// copies all order rows for the company with ID=567 to GBQ.
$qpList = new QueryParamConfigList('tg_orders');
$qpList->add(new QueryParamConfig('companyId', '567', \Doctrine\DBAL\Types\Type::INTEGER));
$mergeService = MergerServiceFactory::createByConfig($config, $qpList);
$mergeService->run('company_567', 'copy rows for company with id=567');

// copies all order rows for the companies with ID=567, 568, 569, 570 in the cycle to GBQ.
$companyIds = array(567, 568, 569, 570)
foreach ($companyIds as $companyId) {
  $qpList = new QueryParamConfigList('tg_orders');
  $qpList->add(new QueryParamConfig('companyId', $companyId, \Doctrine\DBAL\Types\Type::INTEGER));
  $mergeService = MergerServiceFactory::createByConfig($config, $qpList);
  $mergeService->run("company_$companyId", "copy rows for company with id=$companyId");
}

// if some orders in sometime will change status to 9,10 or 11 that you can load this orders by command update.
$updaterService = UpdaterServiceFactory::createByConfig($config);
$updaterService->run();

// if some orders in sometime will change status to 1,2 or 3 that you can load this orders by command update.
$qpList = new QueryParamConfigList('tg_orders');
$qpList->add(new QueryParamConfig('companyId', '567', \Doctrine\DBAL\Types\Type::INTEGER));
$qpList->add(new QueryParamConfig('statuses', '1,2,3', \Doctrine\DBAL\Types\Type::STRING));
$updaterService = UpdaterServiceFactory::createByConfig($config, $qpList);
$updaterService->run();

// if some orders in sometime were created that you can load this orders by command update.
$companyIds = array(567, 568, 569, 570)
foreach ($companyIds as $companyId) {
  $qpList = new QueryParamConfigList('tg_orders');
  $qpList->add(new QueryParamConfig('companyId', $companyId, \Doctrine\DBAL\Types\Type::INTEGER));
  $mergeService = UpdaterServiceFactory::createByConfig($config, $qpList);
  $mergeService->run("company_$companyId", "update rows for company with id=$companyId");
}

```

## Documentation

- [Google BigQuery](https://cloud.google.com/bigquery/docs)
- [Google Cloud Platform](https://cloud.google.com/)
- [Doctrine](https://www.doctrine-project.org/)

### Requirements

- MySql to Google BigQuery works with PHP 5.5 or above.

### Submitting bugs and feature requests

Bugs and feature request are tracked on [GitLab](https://gitlab.com/dashboard/issues?assignee_username=antonioo83)

### Framework Integrations

- Frameworks and libraries using [PSR-4](https://www.php-fig.org/psr/psr-4/)
  can be used very easily with Google Geo API.
- [Symfony](http://symfony.com);
- [Zend](https://framework.zend.com/);
- [Yii](https://www.yiiframework.com/) and other.

### Author

Anton Yurchenko - <antonioo83@mail.ru>

### License

MySql to Google BigQuery is licensed under the MIT License - see the `LICENSE` file for details


