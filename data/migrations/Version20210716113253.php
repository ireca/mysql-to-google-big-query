<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210716113253 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable('mq_journal');
        $table->addColumn('itemCount', \Doctrine\DBAL\Types\Type::BIGINT, array('notnull'  => true, 'default' => 0));
        $table->addColumn('isSuccess', \Doctrine\DBAL\Types\Type::BOOLEAN, array('notnull'  => true, 'default' => true));
        $table->addColumn('errorMessage', \Doctrine\DBAL\Types\Type::STRING, array('length' => 1000, 'notnull'  => true, 'default' => ''));
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $table = $schema->getTable('mq_journal');
        $table->dropColumn('itemCount');
        $table->dropColumn('isSuccess');
        $table->dropColumn('errorMessage');
    }

}
