<?php
namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210531135125 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->connection->executeQuery("
            CREATE TABLE `mq_journal` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `tableName` varchar(100) NOT NULL DEFAULT '',
              `lastPk` bigint(20) NOT NULL,
              `createdAt` datetime NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('`mq_journal`');
    }
}
