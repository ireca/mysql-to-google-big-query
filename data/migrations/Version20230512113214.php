<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20230512113214 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable('mq_journal');
        $table->addColumn(
            'description',
            \Doctrine\DBAL\Types\Type::STRING,
            array('notnull'  => true, 'default' => '', 'length' => '1000')
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $table = $schema->getTable('mq_journal');
        $table->dropColumn('description');
    }
}
