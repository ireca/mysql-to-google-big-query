<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20230406152641 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable('mq_journal');
        $table->addColumn(
            'commandType',
            \Doctrine\DBAL\Types\Type::STRING,
            array('notnull'  => true, 'default' => 'create', 'length' => '20')
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $table = $schema->getTable('mq_journal');
        $table->dropColumn('type');
    }
}
