<?php
namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20230421195108 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @return void
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable('mq_journal');
        $table->addColumn(
            'firstPk',
            \Doctrine\DBAL\Types\Type::BIGINT,
            array('notnull'  => true, 'length' => '20')
        );
    }

    /**
     * @param Schema $schema
     * @return void
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function down(Schema $schema)
    {
        $table = $schema->getTable('mq_journal');
        $table->dropColumn('firstPk');
    }

}
